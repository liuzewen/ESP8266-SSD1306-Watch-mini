**文件夹解释：**

#####  1.``` ESP8266_weather_arduino```

你可以使用**Arduino IDE**直接打开``` ESP8266_weather_arduino```文件夹下的```ESP8266_weather_arduino.ino```文件,并安装所需要的库文件，选择开发板为**NodeMCU 1.0**，编译上传到你的ESP8266开发板即可。

需要安装的库文件有（下载这些库解压后放在你的**Arduino**安装目录下的**libraries**文件夹下面）：

**DS3231**  https://github.com/rodan/ds3231

**FastLED**  https://github.com/FastLED/FastLED

**Time-master** https://github.com/PaulStoffregen/Time

**U8g2**  https://github.com/olikraus/U8g2_Arduino

**ArduinoJson V5**   https://github.com/bblanchon/ArduinoJson/tree/5.x

##### 2.``` ESP8266_weather_PIO```

这是一个标准的 **PlatformIO ESP8266 **工程文件，你需要在你的**VS Code**软件中安装**PlatformIO**插件，并在其中安装ESP8266的板级支持包，然后将``` ESP8266_weather_PIO```添加进工作区编译上传到你的ESP8266开发板即可。

